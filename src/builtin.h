#ifndef BUILTIN_H
#define BUILTIN_H

void simple_echo(char *argv[], int argc);

void echo_n(char *argv[], int argc);

int my_echo(char *argv[], int argc);

int my_cd(char *argv[], int argc);

int my_exit(char *argv[], int argc);

#endif
