#ifndef PARSER_H
#define PARSER_H

#include <stdio.h>
#include "lexer.h"

struct command
{
    int line;
    size_t size;
    size_t capacity;
    char *string;
    struct token **tab;
};

struct node
{
    struct command *cmd;
    struct node *next;
};

struct list
{
    struct node *head;
    struct node *last;
};

// Initializers
struct command *init_cmd(void);
struct list *init_list(void);

//add a token in the tab in command struct
void add_token(struct command *cmd, struct token *tok);

//Check if the list of tokens is logic and respect grammar
int parse(struct lexer *lexer, struct list *cmd_list);

//Add a command in the list of commands
void push_cmd(struct list *cmd_list, struct command *cmd);

char *to_string(struct command *cmd);

void parse_cmd(struct command *cmd);
void free_cmd(struct command *cmd);
void free_nodep(struct node* nd);
void free_list(struct list* lst);

#endif