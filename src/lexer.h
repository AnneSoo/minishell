#ifndef LEXER_H
#define LEXER_H

#include "token.h"

struct lexer
{
    char *string;
    struct lexer_node *head;
    struct lexer_node *last;
};

struct lexer_node
{
    struct token *tok;
    struct lexer_node *next;
};

struct tok_info
{
    char *string;
    size_t size;
    enum tok_type type;
};

//create and return a lexer with head and last pointing on NULL
struct lexer *init_lexer(char *string);

void push_node(struct lexer *lexer, struct token *tok);

//Fills the lexer with tokens (linked list)
void lex(struct lexer *lexer);

void free_node(struct lexer_node *node);

//Destroy the lexer, freeing it
void free_lexer(struct lexer *lex);

#endif