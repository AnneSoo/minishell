#include <stdio.h>
#include <stdlib.h>

#include "builtin.h"


int only_numb(char *str)
{
    char *tmp = str;
    if ((*tmp == '-') || (*tmp == '+'))
        tmp++;
    while (*tmp != '\0')
    {
        if ((*tmp < 48) || (*tmp > 57))
            return 1;
        tmp++;
    }
    return 0;
}

int my_exit(char *argv[], int argc)
{
    if (argc == 1)
        return 0;
    if (only_numb(argv[1]) == 1)
    {
        fprintf(stderr, "minishell: exit: %s: numeric argument required\n",
                argv[1]);
        return 2;
    }
    if (argc > 2)
    {
        fprintf(stderr, "minishell: exit: too many arguments\n");
        return 1;
    }

    int ret = atoi(argv[1]);
    return ret;
}
