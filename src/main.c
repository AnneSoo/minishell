#define _GNU_SOURCE
#define _POSIX_C_SOURCE 200112L

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>


#include "builtin.h"
#include "exec.h"
#include "parser.h"

int shell_interactif(char **command, size_t *bufsize, int *ret)
{
    do
    {
        fprintf(stderr, "minishell$ ");
        getline(command, bufsize, stdin);
        struct lexer *lexer = init_lexer(*command);
        lex(lexer);
        struct list *cmds = init_list();
        int r_parse = parse(lexer, cmds);
        if (r_parse != 0)
        {
            free_lexer(lexer);
            free_list(cmds);
            *ret = 2;
        }
        else
        {
            struct node *tmp = cmds->head;
            while (tmp)
            {
                *ret = execute(tmp->cmd);
                char env[5];
                gcvt(*ret, 0, env);
                setenv("$?", env, 1);
                tmp = tmp->next;
            }
            free_lexer(lexer);
            free_list(cmds);
        }
    } while ((strncmp(*command, "exit\n", 4) != 0) || (*ret == 1));
    return 0;
}

int shell_pipe(char **command, size_t *bufsize, int *ret)
{
    getline(command, bufsize, stdin);
    while (command && *command)
    {
        struct lexer *lexer = init_lexer(*command);
        lex(lexer);
        struct list *cmds = init_list();
        int r_parse = parse(lexer, cmds);
        if (r_parse != 0)
        {
            free_lexer(lexer);
            free_list(cmds);
            return 2;
        }
        struct node *tmp = cmds->head;
        while (tmp)
        {
            *ret = execute(tmp->cmd);
            tmp = tmp->next;
        }
        free_lexer(lexer);
        free_list(cmds);
        free(command);
        command = NULL;
        getline(command, bufsize, stdin);
    }
    return 0;
}


int shell_file(char *file, char *command)
{
    FILE *f = fopen(file, "r");
    int line = 1;
    size_t bufsize = 0;
    int ret_value = 0;
    while (getline(&command, &bufsize, f) != -1)
    {
        struct lexer *lexer = init_lexer(command);
        lex(lexer);
        struct list *cmds = init_list();
        int r_parse = parse(lexer, cmds);
        if (r_parse != 0)
        {
            free_lexer(lexer);
            free_list(cmds);
            ret_value = r_parse;
            break;
        }
        struct node *tmp = cmds->head;
        while (tmp)
        {
            if (tmp->cmd->size != 0)
            {
                tmp->cmd->line = line;
                ret_value = execute(tmp->cmd);
            }
            else
                ret_value = 0;
            char env[5];
            gcvt(ret_value, 0, env);
            setenv("$?", env, 1);
            tmp = tmp->next;
        }
        free_lexer(lexer);
        free_list(cmds);
        free(command);
        command = NULL;
        line++;
    }
    fclose(f);
    free(command);
    return ret_value;
}

int main(int argc, char *argv[])
{
    setenv("$?", "0", 0);
    char *command = NULL;
    size_t bufsize = 0;
    int ret_value = 0;
    if (!isatty(STDIN_FILENO))
    {
        int line = 1;
        while (getline(&command, &bufsize, stdin) != -1)
        {
            struct lexer *lexer = init_lexer(command);
            lex(lexer);
            struct list *cmds = init_list();
            int r_parse = parse(lexer, cmds);
            if (r_parse != 0)
            {
                free_lexer(lexer);
                free_list(cmds);
                ret_value = r_parse;
                break;
            }
            struct node *tmp = cmds->head;
            while (tmp)
            {
                tmp->cmd->line = line;
                ret_value = execute(tmp->cmd);
                char env[5];
                gcvt(ret_value, 0, env);
                setenv("$?", env, 1);
                tmp = tmp->next;
            }
            free_lexer(lexer);
            free_list(cmds);
            free(command);
            command = NULL;
            line++;
        }
    }
    else if (argc == 1) //mode interactif
    {
        int res_inter = shell_interactif(&command, &bufsize, &ret_value);
        if (res_inter != 0)
            ret_value = res_inter;
    }

    else
    {
        int res_file = shell_file(argv[1], command);
        if (res_file != 0)
            ret_value = res_file;
    }

    free(command);
    return ret_value;
}
