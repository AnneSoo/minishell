#ifndef TOKEN_H
#define TOKEN_H

enum tok_type
{
    DAND,
    DGREAT,
    DLESS,
    DOR,
    DELIM,
    GREAT,
    LESS,
    LINE,
    OR,
    SEMI,
    WORD,
};

struct token
{
    enum tok_type type;
    char *str;
};

//Initialize a token and return a pointer to it
struct token *init_token(enum tok_type type, char *string, int size);

//Destroy a token
void free_tok(struct token *tok);

#endif
