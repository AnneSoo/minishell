#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include "exec.h"
#include "builtin.h"

typedef int (*built)(char**, int);

int exec_cmd(char **cmd, int line)
{
    pid_t pid;
    int status;
    if ((pid = fork()) == -1)
    {
        fprintf(stderr, "Error when trying to fork\n");
        return 1;
    }
    else if (pid == 0)
    {
        execvp(cmd[0], cmd);
        if (line != 0)
            fprintf(stderr, "minishell: line %d: %s: command not found\n",
                    line, cmd[0]);
        else
            fprintf(stderr, "minishell: %s: command not found\n", cmd[0]);
        exit(127);
    }
    else
    {
        waitpid(pid, &status, 0);
        if (status != 0)
            status = status/255;
        return status;
    }
}

struct node *handle_dand(struct command *cmd)
{
    struct node *splitted = malloc(sizeof(struct node));
    splitted->cmd = init_cmd();
    splitted->next = malloc(sizeof(struct node));
    splitted->next->cmd = init_cmd();
    splitted->next->next = NULL;
    for (size_t j = 1; j < cmd->size; j++)
    {
        if (cmd->tab[j]->type == DAND)
        {
            for (size_t k = 0; k < j; k++)
            {
                add_token(splitted->cmd, cmd->tab[k]);
            }
            j++;
            for (; j < cmd->size; j++)
            {
                add_token(splitted->next->cmd, cmd->tab[j]);
            }
            break;
        }
    }
    if (splitted->cmd->size != 0)
    {
        splitted->cmd->string = to_string(splitted->cmd);
        splitted->next->cmd->string = to_string(splitted->next->cmd);
        return splitted;
    }
    free_nodep(splitted);
    return NULL;

}

struct node *handle_dor(struct command *cmd)
{
    struct node *splitted = malloc(sizeof(struct node));
    splitted->cmd = init_cmd();
    splitted->next = malloc(sizeof(struct node));
    splitted->next->cmd = init_cmd();
    splitted->next->next = NULL;
    for (size_t j = 1; j < cmd->size; j++)
    {
        if (cmd->tab[j]->type == DOR)
        {
            for (size_t k = 0; k < j; k++)
            {
                add_token(splitted->cmd, cmd->tab[k]);
            }
            j++;
            for (; j < cmd->size; j++)
            {
                add_token(splitted->next->cmd, cmd->tab[j]);
            }
            break;
        }
    }
    if (splitted->cmd->size != 0)
    {
        splitted->cmd->string = to_string(splitted->cmd);
        splitted->next->cmd->string = to_string(splitted->next->cmd);
        return splitted;
    }
    free_nodep(splitted);
    return NULL;
}

struct node *handle_red(struct command *cmd, int *type_red)
{
    struct node *splitted = malloc(sizeof(struct node));
    splitted->cmd = init_cmd();
    splitted->next = malloc(sizeof(struct node));
    splitted->next->cmd = init_cmd();
    splitted->next->next = NULL;
    size_t last = 0;
    int found = 0;
    for (size_t j = 0; j < cmd->size; j++)
    {
        if (cmd->tab[j]->type == GREAT)
        {
            last = j;
            found = 1;
            *type_red = 1;
            break;
        }
        else if (cmd->tab[j]->type == DGREAT)
        {
            last = j;
            found = 1;
            *type_red = 2;
            break;
        }
    }
    if (found == 1)
    {
        for (size_t k = 0; k < last; k++)
        {
            add_token(splitted->cmd, cmd->tab[k]);
        }
        last++;
        for (; last < cmd->size; last++)
        {
            add_token(splitted->next->cmd, cmd->tab[last]);
        }
    }

    if ((splitted->cmd->size != 0) || (found == 1))
    {
        splitted->cmd->string = to_string(splitted->cmd);
        splitted->next->cmd->string = to_string(splitted->next->cmd);
        return splitted;
    }
    free_nodep(splitted);
    return NULL;
}

struct node *handle_pip(struct command *cmd)
{
    struct node *splitted = malloc(sizeof(struct node));
    splitted->cmd = init_cmd();
    splitted->next = malloc(sizeof(struct node));
    splitted->next->cmd = init_cmd();
    splitted->next->next = NULL;
    for (size_t j = 1; j < cmd->size; j++)
    {
        if (cmd->tab[j]->type == OR)
        {
            for (size_t k = 0; k < j; k++)
            {
                add_token(splitted->cmd, cmd->tab[k]);
            }
            j++;
            for (; j < cmd->size; j++)
            {
                add_token(splitted->next->cmd, cmd->tab[j]);
            }
            break;
        }
    }
    if (splitted->cmd->size != 0)
    {
        splitted->cmd->string = to_string(splitted->cmd);
        splitted->next->cmd->string = to_string(splitted->next->cmd);
        return splitted;
    }
    free_nodep(splitted);
    return NULL;
}

int exec_and(struct node *split)
{
    int val1 = execute(split->cmd);
    if (val1 != 0)
    {
        free_nodep(split);
        return val1;
    }
    int val2 = execute(split->next->cmd);
    free_nodep(split);
    return val2;
}

int exec_or(struct node *split)
{
    int val = execute(split->cmd);
    if (val != 0)
    {
        val = execute(split->next->cmd);
        free_nodep(split);
        return val;
    }
    free_nodep(split);
    return val;
}

int exec_red(struct node *split, int *type_red)
{
    int saved_stdout = dup(STDOUT_FILENO);
    int fd = -1;
    if (*type_red == 1)
        fd = open(split->next->cmd->tab[0]->str, O_CREAT|O_WRONLY|O_TRUNC,
                  0666);
    else if (*type_red == 2)
        fd = open(split->next->cmd->tab[0]->str, O_CREAT|O_WRONLY|O_APPEND,
                  0666);
    dup2(fd, STDOUT_FILENO);
    if (split->next->cmd->size > 1)
    {
        for(size_t i = 1; i < split->next->cmd->size; i++)
        {
            add_token(split->cmd, split->next->cmd->tab[i]);
        }
    }
    int val = execute(split->cmd);
    dup2(saved_stdout, STDOUT_FILENO);
    close(fd);
    close(saved_stdout);
    free_nodep(split);
    return val;
}

int exec_pip(struct node *split)
{
    int p[2];
    pipe(p);
    int saved_stdout = dup(STDOUT_FILENO);
    int saved_stdin = dup(STDIN_FILENO);
    pid_t pid;
    int val;
    if ((pid = fork()) == -1)
    {
        fprintf(stderr, "Error when trying to fork\n");
        return 1;
    }
    else if (pid == 0)
    {
        dup2(p[1], STDOUT_FILENO);
        close(p[0]);
        execute(split->cmd);
        close(p[1]);
        abort();
    }
    else
    {
        dup2(p[0], STDIN_FILENO);
        close(p[1]);
        val = execute(split->next->cmd);
        close(p[0]);
    }
    dup2(saved_stdout, STDOUT_FILENO);
    dup2(saved_stdin, STDIN_FILENO);
    close(saved_stdin);
    close(saved_stdout);
    free_nodep(split);
    return val;
}

int execute(struct command *cmd)
{
    char *tab[cmd->size + 1];
    int ret_val = 0;
    int type_red = 0;
    for (size_t i = 0; i < cmd->size; i++)
    {
        tab[i] = cmd->tab[i]->str;
    }
    tab[cmd->size] = NULL;
    struct node *split = handle_dand(cmd);
    if (split)
        return exec_and(split);
    else
    {
        struct node *split = handle_dor(cmd);
        if (split)
            return exec_or(split);
        else
        {
            struct node *split = handle_red(cmd, &type_red);
            if (split)
                return exec_red(split, &type_red);
            else
            {
                struct node *split = handle_pip(cmd);
                if (split)
                    return exec_pip(split);
            }

            int fl = 0;
            char *builtins[3] = {"echo", "cd", "exit"}; //CODING STYLE!!!
            static built builtins_func[] = {my_echo, my_cd, my_exit};

            if (cmd->size == 0)
                return 0;
            char *cmd_name = cmd->tab[0]->str;
            for (int i = 0; i < 3; i++)
            {
                if (strcmp(cmd_name, builtins[i]) == 0)
                {
                    ret_val = builtins_func[i](tab, cmd->size);
                    fl = 1;
                }
            }
            if (fl == 0)
            {
                fflush(stdout);
                fflush(stderr);
                ret_val = exec_cmd(tab, cmd->line);
            }
            return ret_val;
        }
    }
}
