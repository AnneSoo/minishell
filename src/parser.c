#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "parser.h"

#define TAB_INIT_SZ 10

struct command *init_cmd(void)
{
    struct command *cmd = malloc(sizeof(struct command));
    cmd->capacity = TAB_INIT_SZ;
    cmd->size = 0;
    cmd->line = 0;
    cmd->string = NULL;
    cmd->tab = malloc(TAB_INIT_SZ * sizeof(struct token *));
    return cmd;
}

struct list *init_list(void)
{
    struct list *lst = malloc(sizeof(struct list));
    lst->head = NULL;
    lst->last = NULL;
    return lst;
}

void add_token(struct command *cmd, struct token *tok)
{
    struct token *my_tok = init_token(tok->type, tok->str, strlen(tok->str));
    while (cmd->size >= cmd->capacity)
    {
        cmd->capacity *= 2;
        cmd->tab = realloc(cmd->tab, cmd->capacity * sizeof(struct token *));
    }
    cmd->tab[cmd->size] = my_tok;
    cmd->size++;
}

void push_cmd(struct list *cmd_list, struct command *cmd)
{
    struct node *nod = malloc(sizeof(struct node));
    nod->cmd = cmd;
    cmd->string = to_string(cmd);
    nod->next = NULL;
    if ((cmd_list->head == NULL) && (cmd_list->last == NULL))
    {
        cmd_list->head = nod;
        cmd_list->last = nod;
    }
    else
    {
        cmd_list->last->next = nod;
        cmd_list->last = cmd_list->last->next;
    }
}

int check_first_tok(struct token *tok)
{
    if ((tok->type == OR) || (tok->type == DOR) || (tok->type == DAND))
        return -1;
    if ((tok->type == SEMI) || (tok->type == DLESS))
        return -1;
    return 0;
}
int error_red(struct lexer_node **tmp, struct command *cmd, int *line)
{
    *tmp = (*tmp)->next;
    if (!(*tmp))
    {
        fprintf(stderr, "minishell: line %d: syntax error: ", *line);
        fprintf(stderr, "unexpected end of file\n");
        free_cmd(cmd);
        return 2;
    }
    else if (((*tmp)->tok->type == DAND) || ((*tmp)->tok->type == DOR)
             || ((*tmp)->tok->type == OR)
             || ((*tmp)->tok->type == DELIM)
             || ((*tmp)->tok->type == GREAT)
             || ((*tmp)->tok->type == DGREAT))
    {
        fprintf(stderr, "minishell: line %d: syntax error ", *line);
        if ((*tmp)->tok->type == DELIM)
            fprintf(stderr, "near unexpected token 'newline'\n");
        else
            fprintf(stderr, "near unexpected token '%s'\n", (*tmp)->tok->str);
        free_cmd(cmd);
        return 2;
    }
    return 0;
}

int fill_cmd(struct command *cmd, struct lexer_node **tmp, int *line)
{
    while (*tmp && ((*tmp)->tok->type != SEMI)
           && (strcmp((*tmp)->tok->str, "\n") != 0))
    {
        char *str_env = getenv((*tmp)->tok->str);
        if (str_env)
        {
            free((*tmp)->tok->str);
            (*tmp)->tok->str = strdup(str_env);
        }
        if (((*tmp)->tok->type == GREAT) || ((*tmp)->tok->type == DGREAT))
        {
            add_token(cmd, (*tmp)->tok);
            if (error_red(tmp, cmd, line) == 2)
                return 2;
        }
        if (((*tmp)->tok->type == DAND) || ((*tmp)->tok->type == DOR)
                || ((*tmp)->tok->type == OR))
        {
            add_token(cmd, (*tmp)->tok);
            *tmp = (*tmp)->next;
            if (!(*tmp))
            {
                fprintf(stderr, "minishell: line %d: syntax error: ", *line);
                fprintf(stderr, "unexpected end of file\n");
                free_cmd(cmd);
                return 2;
            }
            else if (((*tmp)->tok->type == DAND) || ((*tmp)->tok->type == DOR)
                     || ((*tmp)->tok->type == OR)
                     || ((*tmp)->tok->type == DELIM))
            {
                fprintf(stderr, "minishell: line %d: syntax error ", *line);
                fprintf(stderr, "near unexpected token '%s'\n",
                        (*tmp)->tok->str);
                free_cmd(cmd);
                return 2;
            }
        }
        else if ((*tmp)->tok->type == DELIM)
        {
            *tmp = (*tmp)->next;
            *line = *line + 1;
        }
        else
        {
            add_token(cmd, (*tmp)->tok);
            *tmp = (*tmp)->next;
        }
    }
    if (*tmp && strcmp((*tmp)->tok->str, "\n") == 0)
        *line = *line + 1;
    return 0;
}

int parse(struct lexer *lexer, struct list *cmd_list)
{
    struct lexer_node *tmp = lexer->head;
    int line = 1;
    while (tmp != NULL)
    {
        struct command *cmd = init_cmd();
        int first = check_first_tok(tmp->tok);
        if (first == -1)
        {
            fprintf(stderr, "minishell: line %d: syntax error near ", line);
            fprintf(stderr, "unexpected token `%s'\n", tmp->tok->str);
            fprintf(stderr, "minishell: line %d: \n", line);
            free_cmd(cmd);
            return 2;
        }
        if (tmp->tok->type == DELIM)
        {
            tmp = tmp->next;
            line++;
            free_cmd(cmd);
        }
        else
        {
            if (fill_cmd(cmd, &tmp, &line) != 0)
                return 2;
            push_cmd(cmd_list, cmd);
            if (tmp)
                tmp = tmp->next;
        }
    }
    return 0;
}

char *to_string(struct command *cmd)
{
    char *string = malloc(sizeof(char));
    string[0] = '\0';
    for (size_t i = 0; i < cmd->size; i++)
    {
        string = realloc(string,
                         (strlen(string) + strlen(cmd->tab[i]->str) + 2)
                         * sizeof(char));
        if (i != 0)
            strcat(string, " ");
        strcat(string, cmd->tab[i]->str);
    }
    return string;
}

void free_cmd(struct command *cmd)
{
    for (size_t i = 0; i < cmd->size; i++)
    {
        free_tok(cmd->tab[i]);
    }
    free(cmd->tab);
    free(cmd->string);
    free(cmd);
}

void free_nodep(struct node* nd)
{
    if (!nd)
        return;
    free_cmd(nd->cmd);
    free_nodep(nd->next);
    free(nd);
}

void free_list(struct list* lst)
{
    if (!lst)
        return;
    free_nodep(lst->head);
    free(lst);
}
