#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "token.h"

struct token *init_token(enum tok_type type, char *string, int size)
{
    struct token *tok = malloc(sizeof(struct token));
    tok->str = malloc((size + 1) * sizeof(char));
    tok->type = type;
    strncpy(tok->str, string, size);
    if (tok->str[size] != '\0')
        tok->str[size] = '\0';
    return tok;
}

void free_tok(struct token *tok)
{
    free(tok->str);
    free(tok);
}