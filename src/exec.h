#ifndef EXEC_H
#define EXEC_H

#include "parser.h"

int exec_cmd(char **cmd, int line);
int execute(struct command *cmd);
struct node *handle_dand(struct command *cmd);
struct node *handle_dor(struct command *cmd);

#endif
