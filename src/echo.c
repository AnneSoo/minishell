#include <stdio.h>
#include <string.h>

#include "builtin.h"

void echo_n(char *argv[], int argc)
{
    int i = 0;
    while (i < argc)
    {
        if (i != 0)
            printf(" ");
        int j = 0;
        while (&argv[i][j] && argv[i][j])
        {
            printf("%c", argv[i][j]);
            j++;
        }
        i++;
    }
}

void simple_echo(char *argv[], int argc)
{
    echo_n(argv, argc);
    printf("\n");
}

int my_echo(char *argv[], int argc)
{
    char **tmp = argv;
    if (argc == 1)
    {
        printf("\n");
        return 0;
    } //nothing to print
    if (strcmp(argv[1], "-n") == 0)
    {
        if (argc == 2)
            return 0;
        tmp = tmp + 2;
        echo_n(tmp, argc - 2);
    }
    else
    {
        tmp++;
        simple_echo(tmp, argc - 1);
    }

    fflush(stdout);
    fflush(stderr);
    return 0;
}
