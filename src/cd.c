#include <stdio.h>
#include <unistd.h>

#include "builtin.h"

int my_cd(char *argv[], int argc)
{
    if (argc == 1)
    {
        return 0;
    }
    if (argc > 2)
    {
        fprintf(stderr, "minishell: cd: too many arguments\n");
        return 1;
    }
    int res = chdir(argv[1]);
    if (res == -1)
    {
        fprintf(stderr, "minishell: cd: %s: No such file or directory\n",
                argv[1]);
        return 1;
    }
    return 0;
}