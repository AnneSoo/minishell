#define _POSIX_C_SOURCE 2

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "lexer.h"

#define TAB_SIZE 10

struct tok_info tokens[] =
{
    {
        "&&", 2, DAND,
    },
    {
        ">>", 2, DGREAT,
    },
    {
        "<<", 2, DLESS,
    },
    {
        "||", 2, DOR,
    },
    {
        " ", 1, DELIM,
    },
    {
        "\n", 1, DELIM,
    },
    {
        ">", 1, GREAT,
    },
    {
        "<", 1, LESS,
    },
    {
        "|", 1, OR,
    },
    {
        ";", 1, SEMI,
    },
};

struct lexer *init_lex(void)
{
    struct lexer *lex = malloc(sizeof(struct lexer));
    lex->head = NULL;
    lex->last = lex->head;
    return lex;
}

struct lexer *init_lexer(char *string)
{
    struct lexer *lex = init_lex();
    lex->string = string;
    return lex;
}

struct tok_info *search(char *str)
{
    for (int i = 0; i < TAB_SIZE; i++)
    {
        if (strncmp(str, tokens[i].string, tokens[i].size) == 0)
            return &tokens[i];
    }
    return NULL;
}

void push_node(struct lexer *lexer, struct token *tok)
{
    struct lexer_node *node = malloc(sizeof(struct lexer_node));
    node->tok = tok;
    node->next = NULL;
    if ((lexer->head == NULL) && (lexer->last == NULL))
    {
        lexer->last = node;
        lexer->head = node;
    }
    else
    {
        lexer->last->next = node;
        lexer->last = lexer->last->next;
    }
}

void lex(struct lexer *lexer)
{
    struct tok_info *token = NULL;
    char *tmp = lexer->string;
    char *tmp_prev = tmp;

    while (tmp && *tmp)
    {
        token = search(tmp);
        if (token)
        {
            if (tmp_prev != tmp)
            {
                struct token *tok = init_token(WORD, tmp_prev, tmp - tmp_prev);
                push_node(lexer, tok);
                tmp_prev = tmp;
            }

            if ((token->type == DELIM) && (strcmp(token->string, " ") == 0))
            {
                tmp = tmp + token->size;
                tmp_prev = tmp;
            }
            else
            {
                struct token *tok = init_token(token->type, tmp, token->size);
                push_node(lexer, tok);
                tmp = tmp + token->size;
                tmp_prev = tmp;
            }
        }
        else
            tmp++;

    }
    if (tmp_prev != tmp)
    {
        struct token *tok = init_token(WORD, tmp_prev, tmp - tmp_prev);
        push_node(lexer, tok);
    }
}

void free_node(struct lexer_node *node)
{
    if (!node)
        return;
    free_tok(node->tok);
    free_node(node->next);
    free(node);
}

void free_lexer(struct lexer *lex)
{
    if (!lex)
        return;
    free_node(lex->head);
    free(lex);
}
