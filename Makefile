CC = gcc
CFLAGS = -Wall -Werror -Wextra -pedantic -std=c99 -g -lreadline -fsanitize=address
#DFLAGS = -fsanitize=address

SRC = src/main.c src/parser.c src/lexer.c src/token.c src/exec.c src/echo.c src/cd.c src/exit.c
OBJS = $(SRC:.c=.o)

all : minishell

minishell: $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS)

clean :
	rm $(OBJS) minishell